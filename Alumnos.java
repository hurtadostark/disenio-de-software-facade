import java.util.List;

//Interface de bajo nivel//

public interface Alumnos {
	public List<String> buscarPersonasXDNI(int numero);
	public List<Integer> buscarDNIXPersona(String nombre);
	public int cantidadAlumnos();
}