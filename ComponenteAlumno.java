
import java.util.*;

public class ComponenteAlumno implements Alumnos{
	
/*Se simula la base de datos de un colegio*/
	
	protected Object[] dniPersonas = {"Aron Hurtado", 74139979, "Jimena Cisneros", 45128898,"Armando Lopez", 78452356,
			"Vania Soto", 89565252, "Eduardo Mesa", 32587958, "Franco Zegarra", 33445566, "Beto Little",54418421,
			"Luis Herrera", 66451281};

//Método para buscar personas ingresando el código de DNI//	

	public List<String> buscarPersonasXDNI(int numero){
		int cantidad = dniPersonas.length /2; 
		List<String> resultado = new ArrayList<String>();
		for(int indice = 0; indice < cantidad; indice++){
			int dni = (Integer) dniPersonas[2*indice+1];
			if(dni == numero){
				resultado.add((String) dniPersonas[2*indice]);
			}
		}
		return resultado;
	}

//Método para buscar DNI ingresando el nombre de las personas//
	
	public List<Integer> buscarDNIXPersona(String nombre){
		int cantidad = dniPersonas.length /2; 
		List<Integer> resultado = new ArrayList<Integer>();
		for(int indice = 0; indice < cantidad; indice++){
			String name = (String) dniPersonas[2*indice];
			if(name == nombre){
				resultado.add((Integer) dniPersonas[2*indice+1]);
			}
		}
		return resultado;
	}
	
//Método para ver la cantidad de alumnos//	

	public int cantidadAlumnos(){
		int cantidad = dniPersonas.length /2; 
		return cantidad;
	}
}
