import java.util.List;

//Interface de alto nivel o Facade(Fachada)

public interface WebServiceColegio {
	public String crearReporte(int indice);
	public List<String> buscarPersonasXDNI(int numero);
	public List<Integer> buscarDNIXPersona(String nombre);
	public int cantidadAlumnos();
}
