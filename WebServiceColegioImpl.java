import java.util.List;

public class WebServiceColegioImpl implements WebServiceColegio{
	
//Clase para que funcione e implemente a la interfaz FACADE//
	
	protected Reportes reporte = new ComponenteReportes();
	protected Alumnos alumno = new ComponenteAlumno();

	public String crearReporte(int indice){
		return reporte.crearReporte(indice);
	}
	
	public List<String> buscarPersonasXDNI(int numero){
		return alumno.buscarPersonasXDNI(numero);
	}
	
	public List<Integer> buscarDNIXPersona(String nombre){
		return alumno.buscarDNIXPersona(nombre);
	}
	
	public int cantidadAlumnos(){
		return alumno.cantidadAlumnos();
	}
	
}
